package com.handibigdata.base.enums;

import com.handibigdata.haf.common.constant.IErrorCode;
import org.springframework.http.HttpStatus;

/**
 * Created by weihan on 2016/6/22.
 */
public enum HqdErrorCode implements IErrorCode {
    /*
     *  '400' : 'Bad Request',  //错误的请求
     *  '401' : 'Unauthorized',  //未经授权
     *  '402' : 'Payment Required',  //付费请求
     *  '403' : 'Forbidden',  //禁止
     *  '404' : 'Not Found',  //没有找到
     *  '405' : 'Method Not Allowed',  //方法不允许
     *  '500' : 'Internal Server Error'  //内部服务器错误
     */

    PARAMETER_ERROR(HttpStatus.BAD_REQUEST, "HQD/PARAMETER_ERROR", "hqd.parameter_error"),
    INNER_ERROR(HttpStatus.NOT_FOUND,"HQD/INNER_ERROR","hqd.inner_error"),
    MODULE_NOT_FOUND(HttpStatus.BAD_REQUEST, "HQD/MODULE_NOT_FOUND", "hqd.module_not_found"),
    ;

    private HttpStatus httpStatus;
    private String code;
    private String message;

    HqdErrorCode(HttpStatus httpStatus, String code, String message) {
        this.httpStatus = httpStatus;
        this.code = code;
        this.message = message;
    }
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
