package com.handibigdata.base.config;

import com.handibigdata.haf.common.config.HafWebMvcConfigurerAdapter;
import com.handibigdata.haf.common.tenant.service.DefaultTenantAutoCreateImpl;
import com.handibigdata.haf.common.tenant.service.TenantAutoCreatable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebConfig extends HafWebMvcConfigurerAdapter {

    @Bean
    public TenantAutoCreatable getTenantAutoCreatable() {
        return new DefaultTenantAutoCreateImpl();
    }
}