package com.handibigdata.base.config;


import com.handibigdata.haf.common.config.AbstractHafWebApplicationInitializer;
import com.handibigdata.haf.common.config.ServiceConfigurerAdapter;
import com.handibigdata.haf.rdb.config.RDBConfigurerAdapter;

public class WebAppInitializer extends AbstractHafWebApplicationInitializer {

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{
                WebConfig.class, ServiceConfigurerAdapter.class,
                RDBConfigurerAdapter.class
        };
    }
}
