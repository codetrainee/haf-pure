package com.handibigdata.base.view;

/**
 * 定义的公共View供@JsonView使用
 * Created by wei han on 2020/02/10.
 */
public class View {

    public interface Eager {
    }

    public interface List extends Eager {
    }

    public interface Detail extends List {
    }

    public interface PC extends Detail{
    }
}
