package com.handibigdata.base.web;


import com.handibigdata.haf.common.security.GuestApi;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by weihan on 2016/10/9.
 */

@RestController
@RequestMapping("/${version}/ping")
public class PingController {

    @GuestApi
    @RequestMapping(method = RequestMethod.GET)
    public Map ping() {
        Map status = new HashMap();
        status.put("status","ok");
        return status;
    }

}
